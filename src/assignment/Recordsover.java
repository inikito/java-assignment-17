package assignment;

//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.*;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;


public class Recordsover extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField score11;
	private JTextField score22;
	private JTextField score44;
	private JTextField score33;
	private JTextField score55;
	private JButton button_1, button;
	private DataInputStream input;
	static Recordsover overframe = new Recordsover();
	int idnumb;
	int avg;
	int score1, score2, score3, score4, score5;
	String Firstnametxt;
	String Lastnametxt;
	String hnumtxt;
	String mnumtxt;
	String Addresstxt;
	String regiontxt;
	String citytxt;
	String ziptxt;
	private JTextField Average;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					overframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Recordsover() {
		
		super ( "Read Client File" );

	      // Open the file
	      try  {
	        input = new DataInputStream( 
	                    new FileInputStream( "records.dat" ) );
	      }
	      catch ( IOException e )  {
	        System.err.println( "File not opened properly\n" +
	                            e.toString( ) );
	        System.exit( 1 );
	      }
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);//centered window
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(null);
		panel.setBackground(new Color(30, 144, 255));
		panel.setBounds(0, 0, 434, 261);
		contentPane.add(panel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(10, 25, 30, 20);
		panel.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(50, 25, 86, 20);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(146, 25, 86, 20);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(242, 25, 86, 20);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(338, 25, 86, 20);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(10, 56, 152, 20);
		panel.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(172, 56, 86, 20);
		panel.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(268, 56, 86, 20);
		panel.add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(364, 56, 49, 20);
		panel.add(textField_8);
		
		JLabel label = new JLabel("Subject 1");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setBounds(52, 113, 55, 14);
		panel.add(label);
		
		score11 = new JTextField();
		score11.setColumns(10);
		score11.setBounds(53, 132, 43, 26);
		panel.add(score11);
		
		JLabel label_1 = new JLabel("Subject 2");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setBounds(117, 113, 55, 14);
		panel.add(label_1);
		
		score22 = new JTextField();
		score22.setColumns(10);
		score22.setBounds(117, 132, 43, 26);
		panel.add(score22);
		
		score44 = new JTextField();
		score44.setColumns(10);
		score44.setBounds(247, 132, 43, 26);
		panel.add(score44);
		
		JLabel label_2 = new JLabel("Subject 4");
		label_2.setHorizontalAlignment(SwingConstants.LEFT);
		label_2.setBounds(247, 113, 55, 14);
		panel.add(label_2);
		
		score33 = new JTextField();
		score33.setColumns(10);
		score33.setBounds(183, 132, 43, 26);
		panel.add(score33);
		
		JLabel label_3 = new JLabel("Subject 3");
		label_3.setHorizontalAlignment(SwingConstants.LEFT);
		label_3.setBounds(182, 113, 55, 14);
		panel.add(label_3);
		
		score55 = new JTextField();
		score55.setColumns(10);
		score55.setBounds(307, 132, 43, 26);
		panel.add(score55);
		
		JLabel label_4 = new JLabel("Subject 5");
		label_4.setHorizontalAlignment(SwingConstants.LEFT);
		label_4.setBounds(306, 113, 55, 14);
		panel.add(label_4);
		
		button = new JButton("Close");
		button.setBounds(10, 227, 89, 23);
		panel.add(button);
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a){
				dispose();
				Recordsread rrecords= new Recordsread();
				rrecords.setVisible(true);
			}
		});
		
		button_1 = new JButton("Next");
		button_1.setBounds(335, 227, 89, 23);
		panel.add(button_1);
		button_1.addActionListener(this);
		
		Average = new JTextField();
		Average.setBounds(173, 185, 86, 20);
		panel.add(Average);
		Average.setColumns(10);
		
		JLabel lblAverage = new JLabel("Average:");
		lblAverage.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAverage.setBounds(77, 188, 86, 14);
		panel.add(lblAverage);
		button_1.addActionListener(this);
	}
	public void over70()
{
		
	    // input the values from the file
	    try  {
	      idnumb = input.readInt();
	      Firstnametxt = input.readUTF();
	      Lastnametxt = input.readUTF();
	      hnumtxt = input.readUTF();
	      mnumtxt = input.readUTF();
	      Addresstxt = input.readUTF();
	      regiontxt = input.readUTF();
	      citytxt = input.readUTF();
	      ziptxt = input.readUTF();
	      score1 = input.readInt();
	      score2 = input.readInt();
	      score3 = input.readInt();
	      score4 = input.readInt();
	      score5 = input.readInt();
	      avg= input.readInt();
	      
	      if ((score1>70)||(score2>70)||(score3>70)||(score4>70)||(score5>70)){
	      
	      textField.setText(String.valueOf(idnumb));
	      textField_1.setText(Firstnametxt);
	      textField_2.setText(Lastnametxt);
	      textField_3.setText(hnumtxt);
	      textField_4.setText(mnumtxt);
	      textField_5.setText(Addresstxt);
	      textField_6.setText(regiontxt);
	      textField_7.setText(citytxt);
	      textField_8.setText(ziptxt);
	      score11.setText(String.valueOf(score1));
	      score22.setText(String.valueOf(score2));
	      score33.setText(String.valueOf(score3));
	      score44.setText(String.valueOf(score4));
	      score55.setText(String.valueOf(score5));
	      Average.setText(String.valueOf(avg));
	    	
	    }
	    }
	    catch ( EOFException eof )  {
	      closeFile( );
	    }
	    catch ( IOException e )  {
	      System.err.println( "Error during read from file\n" +
	                          e.toString( ) );
	      System.exit( 1 );
	    }
	  }
	  
	  public void closeFile( )  {
	    try  {
	      input.close( );
	      dispose();
	      Recordsread rrecords= new Recordsread();
		  rrecords.setVisible(true);
	    }
	    catch( IOException e )  {
	      System.err.println( "Error closing file\n" +
	                            e.toString( ) );
	        System.exit( 1 );
	    } 
	  }
	  
		public void actionPerformed(ActionEvent e) {
			   if ( e.getSource() == button_1 )
				   over70();
			     else closeFile();
			  }
	  
	  public static void main1 ( String args[ ] )
	  {
	     new Recordsover( );
	  }
}