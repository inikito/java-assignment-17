package assignment;

//import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JButton;
//import javax.swing.JSplitPane;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import java.io.*;
//import java.awt.*;
//import java.awt.event.*;


public class Recordsread extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	@SuppressWarnings("unused")
	private DataInputStream input;
	static Recordsread rrecord = new Recordsread();
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					rrecord.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Recordsread() {
		setResizable(false);
		setBackground(new Color(30, 144, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 210, 326);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(30, 144, 255));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);//centered window
		
		JButton btnNewButton = new JButton("Subject(s) over 70");
		btnNewButton.setBounds(28, 143, 140, 31);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a){
				dispose();
				Recordsover overframe= new Recordsover();
				overframe.setVisible(true);
			}
		});
		
		JButton btnAverageAll = new JButton("Average All");
		btnAverageAll.setBounds(28, 88, 140, 31);
		contentPane.add(btnAverageAll);
		btnAverageAll.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent e)
		    {
		    	dispose();
		    	Recordsaverage averframe= new Recordsaverage();
				averframe.setVisible(true);
		    }
		});
		
		JButton btnSubjectsLess = new JButton("Subject(s) less 40");
		btnSubjectsLess.setBounds(28, 200, 140, 31);
		contentPane.add(btnSubjectsLess);
		btnSubjectsLess.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent a){
				dispose();
				Recordsunder underframe= new Recordsunder();
				underframe.setVisible(true);
			}
		});
		
		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(30, 144, 255));
		separator.setForeground(new Color(192, 192, 192));
		separator.setBounds(0, 33, 194, 2);
		contentPane.add(separator);
		
		JLabel lblReadRecords = new JLabel("Read Records");
		lblReadRecords.setHorizontalAlignment(SwingConstants.CENTER);
		lblReadRecords.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		lblReadRecords.setBounds(0, 11, 194, 14);
		contentPane.add(lblReadRecords);
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.setBounds(51, 253, 89, 23);
		contentPane.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener(){
		    //@Override
		    public void actionPerformed(ActionEvent f)
		    {
		    	dispose();
		    	SB sbframe= new SB();
				sbframe.setVisible(true);
		    }
		});
	}
}