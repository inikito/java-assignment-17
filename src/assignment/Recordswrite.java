package assignment;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;

public class Recordswrite extends JFrame implements ActionListener {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField Lastnametxt, Addresstxt, Firstnametxt, idtxt, citytxt, ziptxt, regiontxt, hnumtxt, mnumtxt, sub1txt, sub2txt, sub3txt, sub4txt, sub5txt, score1txt, score2txt, score3txt, score4txt, score5txt;
   private JTextField averagetxt;
	private JTextField statustxt;
	private DataOutputStream output;
	@SuppressWarnings("unused")
	private JButton btnNewButton, btnCancel, btnLogout, clearbtn;
	int avg=0;
    int score1=0;
    int score2=0;
    int score3=0;
    int score4=0;
    int score5=0;
    int sum;
    int avar;
    static Recordswrite rwframe = new Recordswrite();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					//Recordswrite fifthframe = new Recordswrite();
					rwframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				//new Recordswrite();
			}
		});

	}

	/**
	 * Create the frame.
	 */
	
	//creating the fileoutputstream and the constructor. I have put the fileoutputstream inside the constructor
	public Recordswrite() {
		super( "Create Client File" );

	     try  {
	       output = new DataOutputStream( 
	                 new FileOutputStream( "records.dat", true ) );
	     }
	     catch ( IOException e )  {
	       System.err.println( "File not open properly\n" +
	         e.toString( ) );
	       System.exit( 1 );
	     }       

		setTitle("CityU Student Records");
		setBackground(new Color(0, 153, 255));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-3, -44, 509, 493);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 153, 255));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);//centered window
		
		Lastnametxt = new JTextField();
		Lastnametxt.setBounds(173, 93, 103, 26);
		contentPane.add(Lastnametxt);
		Lastnametxt.setColumns(10);
		
		JLabel header = new JLabel("CREATE A NEW RECORD");
		header.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		header.setHorizontalAlignment(SwingConstants.CENTER);
		header.setBounds(0, 13, 503, 14);
		contentPane.add(header);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 38, 505, 1);
		contentPane.add(separator);
		
		Addresstxt = new JTextField();
		Addresstxt.setColumns(10);
		Addresstxt.setBounds(10, 145, 195, 26);
		contentPane.add(Addresstxt);
		
		Firstnametxt = new JTextField();
		Firstnametxt.setColumns(10);
		Firstnametxt.setBounds(64, 93, 100, 26);
		contentPane.add(Firstnametxt);
		
		JLabel lblStudentId = new JLabel("First Name");
		lblStudentId.setHorizontalAlignment(SwingConstants.LEFT);
		lblStudentId.setBounds(67, 79, 97, 14);
		contentPane.add(lblStudentId);
		
		JLabel lblStudentId_1 = new JLabel(" ID");
		lblStudentId_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblStudentId_1.setBounds(10, 79, 44, 14);
		contentPane.add(lblStudentId_1);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setHorizontalAlignment(SwingConstants.LEFT);
		lblLastName.setBounds(176, 79, 100, 14);
		contentPane.add(lblLastName);
		
		idtxt = new JTextField();
		idtxt.setColumns(6);
		idtxt.setBounds(10, 93, 44, 26);
		contentPane.add(idtxt);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setHorizontalAlignment(SwingConstants.LEFT);
		lblAddress.setBounds(13, 130, 189, 14);
		contentPane.add(lblAddress);
		
		citytxt = new JTextField();
		citytxt.setColumns(10);
		citytxt.setBounds(335, 145, 91, 26);
		contentPane.add(citytxt);
		
		JLabel lblCity = new JLabel("City");
		lblCity.setHorizontalAlignment(SwingConstants.LEFT);
		lblCity.setBounds(339, 130, 87, 14);
		contentPane.add(lblCity);
		
		ziptxt = new JTextField();
		ziptxt.setColumns(10);
		ziptxt.setBounds(436, 145, 54, 26);
		contentPane.add(ziptxt);
		
		JLabel lblPostcode = new JLabel(" ZipCode");
		lblPostcode.setHorizontalAlignment(SwingConstants.LEFT);
		lblPostcode.setBounds(437, 130, 62, 14);
		contentPane.add(lblPostcode);
		
		regiontxt = new JTextField();
		regiontxt.setColumns(10);
		regiontxt.setBounds(215, 145, 109, 26);
		contentPane.add(regiontxt);
		
		JLabel lblAddress_1 = new JLabel(" Region");
		lblAddress_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblAddress_1.setBounds(215, 130, 108, 14);
		contentPane.add(lblAddress_1);
		
		hnumtxt = new JTextField();
		hnumtxt.setColumns(10);
		hnumtxt.setBounds(286, 93, 92, 26);
		contentPane.add(hnumtxt);
		
		JLabel label = new JLabel("Home Number");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setBounds(289, 78, 89, 14);
		contentPane.add(label);
		
		mnumtxt = new JTextField();
		mnumtxt.setColumns(10);
		mnumtxt.setBounds(386, 93, 104, 26);
		contentPane.add(mnumtxt);
		
		JLabel label_1 = new JLabel("Mobile Number");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setBounds(389, 78, 101, 14);
		contentPane.add(label_1);
		
		JLabel lblStudentDetails = new JLabel("   Student's Details");
		lblStudentDetails.setForeground(Color.GRAY);
		lblStudentDetails.setHorizontalAlignment(SwingConstants.LEFT);
		lblStudentDetails.setFont(new Font("Arial", Font.PLAIN, 13));
		lblStudentDetails.setBounds(0, 50, 503, 14);
		contentPane.add(lblStudentDetails);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(Color.GRAY);
		separator_1.setBounds(11, 66, 476, 1);
		contentPane.add(separator_1);
		
		JLabel lblStudentsSubjects = new JLabel("   Student's Subjects");
		lblStudentsSubjects.setHorizontalAlignment(SwingConstants.LEFT);
		lblStudentsSubjects.setForeground(Color.GRAY);
		lblStudentsSubjects.setFont(new Font("Arial", Font.PLAIN, 13));
		lblStudentsSubjects.setBounds(0, 194, 503, 14);
		contentPane.add(lblStudentsSubjects);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBackground(Color.GRAY);
		separator_2.setBounds(11, 210, 476, 1);
		contentPane.add(separator_2);
		
		sub1txt = new JTextField();
		sub1txt.setEditable(false);
		sub1txt.setText("BCO4001");
		sub1txt.setColumns(10);
		sub1txt.setBounds(10, 240, 164, 26);
		contentPane.add(sub1txt);
		
		JLabel lblSubject = new JLabel("Subject 1");
		lblSubject.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubject.setBounds(13, 225, 161, 14);
		contentPane.add(lblSubject);
		
		score1txt = new JTextField();
		score1txt.setColumns(10);
		score1txt.setBounds(172, 240, 43, 26);
		contentPane.add(score1txt);
		
		JLabel lblAlgebra = new JLabel("Score");
		lblAlgebra.setHorizontalAlignment(SwingConstants.LEFT);
		lblAlgebra.setBounds(175, 225, 40, 14);
		contentPane.add(lblAlgebra);
		
		JLabel lblSubject_1 = new JLabel("Subject 4");
		lblSubject_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubject_1.setBounds(242, 225, 161, 14);
		contentPane.add(lblSubject_1);
		
		sub4txt = new JTextField();
		sub4txt.setText("BCO4004");
		sub4txt.setEditable(false);
		sub4txt.setColumns(10);
		sub4txt.setBounds(239, 240, 164, 26);
		contentPane.add(sub4txt);
		
		score4txt = new JTextField();
		score4txt.setColumns(10);
		score4txt.setBounds(402, 240, 43, 26);
		contentPane.add(score4txt);
		
		JLabel label_3 = new JLabel("Score");
		label_3.setHorizontalAlignment(SwingConstants.LEFT);
		label_3.setBounds(405, 225, 40, 14);
		contentPane.add(label_3);
		
		JLabel lblSubject_2 = new JLabel("Subject 2");
		lblSubject_2.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubject_2.setBounds(13, 277, 161, 14);
		contentPane.add(lblSubject_2);
		
		sub2txt = new JTextField();
		sub2txt.setEditable(false);
		sub2txt.setText("BCO4002");
		sub2txt.setColumns(10);
		sub2txt.setBounds(10, 292, 164, 26);
		contentPane.add(sub2txt);
		
		score2txt = new JTextField();
		score2txt.setColumns(10);
		score2txt.setBounds(173, 292, 43, 26);
		contentPane.add(score2txt);
		
		JLabel label_5 = new JLabel("Score");
		label_5.setHorizontalAlignment(SwingConstants.LEFT);
		label_5.setBounds(176, 277, 40, 14);
		contentPane.add(label_5);
		
		JLabel lblSubject_3 = new JLabel("Subject 5");
		lblSubject_3.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubject_3.setBounds(242, 277, 161, 14);
		contentPane.add(lblSubject_3);
		
		sub5txt = new JTextField();
		sub5txt.setText("BCO4005");
		sub5txt.setEditable(false);
		sub5txt.setColumns(10);
		sub5txt.setBounds(239, 292, 164, 26);
		contentPane.add(sub5txt);
		
		score5txt = new JTextField();
		score5txt.setColumns(10);
		score5txt.setBounds(402, 292, 43, 26);
		contentPane.add(score5txt);
		
		JLabel label_7 = new JLabel("Score");
		label_7.setHorizontalAlignment(SwingConstants.LEFT);
		label_7.setBounds(405, 277, 40, 14);
		contentPane.add(label_7);
		
		JLabel lblSubject_4 = new JLabel("Subject 3");
		lblSubject_4.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubject_4.setBounds(13, 329, 161, 14);
		contentPane.add(lblSubject_4);
		
		sub3txt = new JTextField();
		sub3txt.setText("BCO4003");
		sub3txt.setEditable(false);
		sub3txt.setColumns(10);
		sub3txt.setBounds(10, 344, 164, 26);
		contentPane.add(sub3txt);
		
		score3txt = new JTextField();
		score3txt.setColumns(10);
		score3txt.setBounds(173, 344, 43, 26);
		contentPane.add(score3txt);
		
		JLabel label_9 = new JLabel("Score");
		label_9.setHorizontalAlignment(SwingConstants.LEFT);
		label_9.setBounds(176, 329, 40, 14);
		contentPane.add(label_9);
		
		averagetxt = new JTextField();
		averagetxt.setEditable(false);
		averagetxt.setColumns(10);
		averagetxt.setBounds(242, 344, 62, 26);
		contentPane.add(averagetxt);
		
		JLabel lblAverage = new JLabel("Average");
		lblAverage.setHorizontalAlignment(SwingConstants.LEFT);
		lblAverage.setBounds(244, 329, 62, 14);
		contentPane.add(lblAverage);
		
		statustxt = new JTextField();
		statustxt.setEditable(false);
		statustxt.setColumns(10);
		statustxt.setBounds(304, 344, 62, 26);
		contentPane.add(statustxt);
		
		JLabel lblStatus = new JLabel("Status");
		lblStatus.setHorizontalAlignment(SwingConstants.LEFT);
		lblStatus.setBounds(306, 329, 62, 14);
		contentPane.add(lblStatus);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.setBounds(398, 430, 89, 23);
		contentPane.add(btnNewButton);
		getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(this);
		
		btnLogout = new JButton("Back");
		btnLogout.setBounds(10, 430, 89, 23);
		contentPane.add(btnLogout);
		btnLogout.addActionListener(new ActionListener(){
		    //@Override
		    public void actionPerformed(ActionEvent f)
		    {
		    	dispose();
		    	SB sbframe= new SB();
				sbframe.setVisible(true);
		    }
		});
		
		
		clearbtn = new JButton("Average");
		clearbtn.setFont(new Font("Tahoma", Font.PLAIN, 11));
		clearbtn.setBounds(374, 346, 97, 23);
		contentPane.add(clearbtn);
		clearbtn.addActionListener(new ActionListener(){
		    @Override
		    public void actionPerformed(ActionEvent g)
		    {
		    	score1=Integer.parseInt( score1txt.getText( ) ) ;
		    	score2=Integer.parseInt( score2txt.getText( ) ) ;
		    	score3=Integer.parseInt( score3txt.getText( ) ) ;
		    	score4=Integer.parseInt( score4txt.getText( ) ) ;
		    	score5=Integer.parseInt( score5txt.getText( ) ) ;
		    	averagetxt.setText(String.valueOf((score1+score2+score3+score4+score5)/5));
		    	avar=Integer.parseInt(averagetxt.getText());
		    	if (avar <=40){
		    		statustxt.setText("FAILED");
		    	}
		    	else{
		    		statustxt.setText("PASSED");
		    	}
		    }
		});
	}
	
	//Created my method.
	public void addRecord()
	{
		int idnumb= 0;
	      
	      //Let's check the student's id. If there's not any, user should insert a integer number.
	      if ( ! idtxt.getText( ).equals( "" ) ){
	        try  {
	        	idnumb = Integer.parseInt( idtxt.getText( ) ) ;
	        }
	        catch ( NumberFormatException nfe )  {
	        	JOptionPane.showMessageDialog (null, "Please enter an integer number for ID", "Uh oh!", JOptionPane.ERROR_MESSAGE);
		        }
	        try {
	        	score1=Integer.parseInt( score1txt.getText( ) ) ;
	        	score2=Integer.parseInt( score2txt.getText( ) ) ;
	        	score3=Integer.parseInt( score3txt.getText( ) ) ;
	        	score4=Integer.parseInt( score4txt.getText( ) ) ;
	        	score5=Integer.parseInt( score5txt.getText( ) ) ;

	        	sum=score1+score2+score3+score4+score5;
		      	avg=sum/5;
	        	if ((score1 <0) || (score1>100))
			     {
		     		JOptionPane.showMessageDialog(null, "Enter a score between 0 to 100 for BCO4001", "Uh oh!", JOptionPane.INFORMATION_MESSAGE);
		     		 }
	        	else if ((score2 <0)|| (score2 >100))
	        	{
	        		JOptionPane.showMessageDialog(null, "Enter a score between 0 to 100 for BCO4002", "Uh oh!", JOptionPane.INFORMATION_MESSAGE);
	        	}
	        	else if ((score3<0)||(score3>100))
	        	{
	        		JOptionPane.showMessageDialog(null, "Enter a score between 0 to 100 for BCO4003", "Uh oh!", JOptionPane.INFORMATION_MESSAGE);
	        	}
	        	else if ((score4<0)||(score4>100))
	        	{
	        		JOptionPane.showMessageDialog(null, "Enter a score between 0 to 100 for BCO4004", "Uh oh!", JOptionPane.INFORMATION_MESSAGE);
	        	}
	        	else if ((score5<0)||(score5>100))
	        	{
	        		JOptionPane.showMessageDialog(null, "Enter a score between 0 to 100 for BCO4005", "Uh oh!", JOptionPane.INFORMATION_MESSAGE);
	        	}
	        	
	        	else if ( idnumb > 0 )  {
	            output.writeInt( idnumb );
	            output.writeUTF( Firstnametxt.getText() );
	            output.writeUTF( Lastnametxt.getText() );
	            output.writeUTF( hnumtxt.getText() );
	            output.writeUTF( mnumtxt.getText() );
	            output.writeUTF( Addresstxt.getText() );
	            output.writeUTF( regiontxt.getText() );
	            output.writeUTF( citytxt.getText() );
	            output.writeUTF( ziptxt.getText() );
	            output.writeInt( score1);
	            output.writeInt( score2);
	            output.writeInt( score3);
	            output.writeInt( score4);
	            output.writeInt( score5);
	            output.writeInt(avg);
	          
	          // clear the text fields
	          Firstnametxt.setText( "" );
	          Lastnametxt.setText( "" );
	          Firstnametxt.setText("");
	            hnumtxt.setText("");
	            idtxt.setText("");
	            mnumtxt.setText("");
	            Addresstxt.setText("");
	            regiontxt.setText("");
	            citytxt.setText("");
	            ziptxt.setText("");
	            score1txt.setText("");
	            score2txt.setText("");
	            score3txt.setText("");
	            score4txt.setText("");
	            score5txt.setText("");
	        }
	        }
	        catch ( IOException io )  {
	          System.err.println( "Error when writing to file\n" +
	            io.toString( ) ) ;
	          System.exit( 1) ;
	        }
	      }
	   }
	
	
	public void actionPerformed( ActionEvent g )  {  
		addRecord();
		if ( g.getSource( ) == btnNewButton)  {
	       try {
	         output.close( );
	       }
	       catch ( IOException io )  {
	         System.err.println( "File not closed properly\n" +
	            g.toString( ) );
	         System.exit( 1 );
	       }
	      
	       System.exit(0);  
	     }
		if (g.getSource()==btnLogout){
			dispose();
		}
}
}